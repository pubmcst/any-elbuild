#!/bin/sh
#
# Copyright (c) 2020, AO MCST. All rights reserved.
# This program and accompanying materials are available under the terms of the 2-clause BSD License.
#

configopts()
{
    printf "%s\n" "
--prefix='${PREFIX}'
--mandir='${PREFIX}/share/man'
--infodir='${PREFIX}/share/info'
--datadir='${PREFIX}/share'
--sysconfdir='/etc'
--localstatedir='/var'
"
    if ! ause 'native' ; then
        printf "%s\n" "--cache-file=./config.cache"
    fi
    if test -n "${CONFIG_TARGET}" ; then
        printf "%s\n" "--host='${CONFIG_TARGET}'"
    fi
}

# форма econf + configopts позволяет использовать в билдах две схемы интерфейсов:
#   либо компактный econf,
#   либо вызов configure и вручную подключение опций по умолчанию из configopts
#   вторая схема более громоздкая, но наглядная

econf()
{
    if use 'cov'; then
        set_cov_opts
    fi

    CONFIGURE="${CONFIGURE:-./configure}"
    if [ -n "${ECONF_SOURCE}" -a -x "${ECONF_SOURCE}/configure" ] ; then
        CONFIGURE="${ECONF_SOURCE}/configure"
    fi
    eval \
    "${CONFIGURE}" \
        $(configopts) \
        "${@}"
}

# если не поддержаны стандартные опции configure
econf_explicit()
{
    if use 'cov'; then
        set_cov_opts
    fi

    CONFIGURE="${CONFIGURE:-./configure}"
    if [ -n "${ECONF_SOURCE}" -a -x "${ECONF_SOURCE}/configure" ] ; then
        CONFIGURE="${ECONF_SOURCE}/configure"
    fi
    eval \
    "${CONFIGURE}" \
        "${@}"
}

emake()
{
    (
    export WRAPCC_VERBOSE="${WRAPCC_VERBOSE:-'1'}"

    if use 'cov'; then
        set_cov_opts
    fi

    UTILMAKE="${UTILMAKE:-make}"
    if [ -n "${MAKE}" ] ; then
        UTILMAKE="${MAKE}"
    fi
    "${UTILMAKE}" ${MAKEOPTS} \
        "${@}"
    )
}

autotools_stop()
{
    export AUTOCONF=:
    export AUTOHEADER=:
    export AUTOMAKE=:
    export ACLOCAL=:
}

autotools_allow()
{
    unset AUTOCONF
    unset AUTOHEADER
    unset AUTOMAKE
    unset ACLOCAL
}

fillenv()
{
    envvar="$1"
    value="$2"

    if [ -z "${envvar}" ] ; then
        die "Bad variable name"
    fi

    printf "%s\n" "export ${envvar}='${value}'"
}

fillenvappend()
{
    envvar="$1"
    value="$2"

    if [ -z "${envvar}" ] ; then
        die "Bad variable name"
    fi

    old_value="\${$envvar}"
    printf "%s\n" "
if ! echo ${old_value} | /bin/grep -q \"${value}\" ; then
    export ${envvar}=${old_value}:${value}
fi
"
}

pkg_env()
{
    envvar="$1"
    value="$2"
    append="$3"

    if [ -z "${envvar}" ] ; then
        die "Bad key for env variable is given"
    fi

    mkdir -p "${D}/etc/profile.d/"
    if [ -n "${append}" ] ; then
        fillenvappend "${envvar}" "${value}" >> "${D}/etc/profile.d/${P}.sh"
    else
        fillenv "${envvar}" "${value}" >> "${D}/etc/profile.d/${P}.sh"
    fi

    chmod +x "${D}/etc/profile.d/${P}.sh"
}

init_header_write()
{
    local service_name="$1"
    local service_path="$2"
    local index=
    eval index="${service_name//[-.]/_}"
    index="${index//[-.]/_}"

    # подключаем конфиг со значениями для данного пакета
    if [ -e ${DISTDIR}/sysv.conf ]; then
        . ${DISTDIR}/sysv.conf
    fi
    # удаляем предыдущий заголовок и пишем актуальный
    sed -i \
        -e "/BEGIN INIT INFO/,/END INIT INFO/d" \
        -e "1 a \
### BEGIN INIT INFO\n\
# Provides:          ${service_name}\n\
# Required-Start:    ${RSTART[$index]}\n\
# Required-Stop:     ${RSTOP[$index]}\n\
# Default-Start:     ${DSTART[$index]}\n\
# Default-Stop:      ${DSTOP[$index]}\n\
# Short-Description:\n\
# Description:\n\
### END INIT INFO" \
    "${service_path}"

}

init_header_change()
{
    local name="$1"
    local location="$2"

    if [ -d "${location}" ] ; then
        location_abs="$(readlink -m "${location}" )"
        ls -qd "${location_abs}/"* | while read initname ; do
            if [ -f "${initname}" ] ; then
                init_header_write "$(basename "${initname}" )" "${initname}"
            fi
        done
    elif [ -f "${location}" ] ; then
        init_header_write "$(basename "$location" )" "${location}"
    else
        die "Incorrect location of init object. Location: ${location}"
    fi
}

image_init_format()
{
    if [ -d "${D}/etc/init.d" ] ; then
        init_header_change ${P} "${D}/etc/init.d"
    fi
}


# слияние библиотек из путей с явным указанием разрядности (lib64, lib32) и пути по умолчанию
image_merge_lib()
{
    mylib="lib${ANYARCHCAP}"
    bindir="${D}"

    for pref in ${PREFIX_LIST} ; do
        if [ ${pref} = '/' ] ; then
            pref=
        fi
    
        if [ -d "${bindir}${pref}/${mylib}" ] ; then
            if [ "$( readlink -m "${bindir}${pref}/lib" )" = "$( readlink -m "${bindir}${pref}/${mylib}" )" ] ; then
                continue
            fi
            mkdir -p "${bindir}${pref}/lib/"
            cp -Rf "${bindir}${pref}/${mylib}/"* "${bindir}${pref}/lib/"
            rm -rf "${bindir}${pref}/${mylib}/"
        fi
    done
}

# спец.директория для хранения настроек

image_install_default_files()
{
    default_source="etc.mcst"
    default_dest="etc"

    if test -d "${S}/${default_source}" -a -d "${D}" ; then
        mkdir -p "${D}/${default_dest}"
        cp -Rf "${S}/${default_source}"/* "${D}/${default_dest}/"
    fi
}
