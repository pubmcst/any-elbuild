#!/bin/bash
#
# Copyright (c) 2020, AO MCST. All rights reserved.
# This program and accompanying materials are available under the terms of the 2-clause BSD License.
#
# variables for Elbrus building

# общие переменные

export LC_ALL=C
export INPUTRC=/etc/inputrc
export XDG_DATA_DIRS=${ROOT}/usr/share:/usr/share/:/usr/local/share/

if ause 'x86' ; then
    export LD_LIBRARY_PATH="${ROOT}/usr/lib:${ROOT}/usr/lib64"
fi

# отключение принудительного autotools
export AUTOCONF=:
export AUTOHEADER=:
export AUTOMAKE=:
export ACLOCAL=:

# переменные дополнительного API либо модификации существующих

export GLIBC_VERSION=${elbuild_glibc_version:-223}

arch_cap=

case "${ANYARCHCAP}" in
    32)
        arch_cap='-m32'
    ;;
    64)
        arch_cap='-m64'
    ;;
    128)
        arch_cap='-m128'
    ;;
esac

ARCH_FLAGS="${arch_cap} ${ARCH_FLAGS}"

# несколько приоритетных дополнительных опций, задающих оптимизации
if [ -n "${option_optforce}" ] ; then
    OPT_FLAGS="${option_optforce}"
elif [ -n "${option_buildmode}" ] ; then
    OPT_FLAGS="${option_buildmode}"
elif [ -n "${option_optmode}" ] ; then
    OPT_FLAGS="${option_optmode}"
fi

AUTOTOOLS_HOST="${CONFIG_HOST}"
AUTOTOOLS_TARGET="${CONFIG_TARGET}"
AUTO_ARCH="${ANYARCHMODE}"

what_to_print_about_atom="${P} |${OPT_FLAGS}|${MAKEOPTS}|"

# поддержка необходимого окружения в изоляции
if [ "${bool_SANDBOX}" = '1' -a -d '/etc/profile.d/' ] ; then
    for i in /etc/profile.d/*.sh ; do
        if [ -r $i ]; then
            . $i
        fi
    done
    unset i
fi

export D S T
export any_rdd_root rdd_atom_path option_src_local_patch
export ANYARCHMODE MAKEOPTS OPT_FLAGS

# данные по ядру

export KERN_ROOT=${elbuild_kernel_root}
export KERN_UPSTREAM_VERSION=${elbuild_kernel_upstream}

if [ "${ANYARCHMODE}" = 'e2k' ] ; then
    KERN_ARCH=${ANYARCHMODE}
else
    KERN_ARCH=${ANYARCH}
fi

export KERN_ARCH
KERN_SUBARCH=${ANYARCH}
if [ -n "${elbuild_kernel_arch}" ] ; then
    KERN_SUBARCH=${elbuild_kernel_arch}
fi
subarchdir="$KERN_ARCH"
if ause 'e1c' ; then
    subarchdir="e1c"
fi

export KERN_INCDIR=${KERN_ROOT}/$subarchdir/linux-include-${KERN_UPSTREAM_VERSION}
export KERN_SRCDIR=${KERN_ROOT}/src
export KERN_BINDIR=${KERN_ROOT}/$subarchdir/bin-linux-${KERN_UPSTREAM_VERSION}

export KERN_BRANCH=${elbuild_kernel_branch}
export KERN_REV=${elbuild_kernel_inner_version}
