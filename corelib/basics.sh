
prepare_conditional_patches()
{
    if ause 'protected' ; then
        if test -d ${PATCHDIR}/protected/ ; then
            dopatch ${PATCHDIR}/protected/
        fi
    fi
}

src_prepare_extend()
{
    prepare_conditional_patches
}

pkg_install()
{
    pkg_preinstall
    
    save_trace_and_hide_verbose

    pkg_install_extend_pre

    image_remove_from_config
    image_install_default_files
    image_init_format
    image_merge_lib

    pkg_install_extend
    restore_trace_state

    pkg_postinstall
}

src_packinstall()
{
    return
}
