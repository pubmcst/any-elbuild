#!/bin/sh

set -o errexit

if test ! -d "${ADIR}" ; then
    printf "%s\n" "Variable ADIR with local working copy is not set. Nowhere to install shell code."
    exit 1
fi

set -o xtrace

mkdir -p \
    "${ADIR}/fs/lib" \
    "${ADIR}/fs/corelib" \
    "${ADIR}/fs/include" \
    "${ADIR}/rdd.conf.d/"

cp -Rf lib/* "${ADIR}/fs/lib"
cp -Rf corelib/* "${ADIR}/fs/corelib"
cp -Rf include/* "${ADIR}/fs/include"
cp -f conf/* "${ADIR}/rdd.conf.d/"
